<%@ page language="java" contentType="text/html; charset=ISO-8859-1" 
import="com.perou.wiki.controleurs.Action,com.perou.wiki.beans.Statut" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Insert title here</title>
</head>
	<jsp:include page="menu.jsp"></jsp:include>
<body>
	<div class="container-fluid">
		<h2>Creation d'article</h2>
		<form method="post" action="ArticleController">
			<div class="form-group">
				Title: <input class="form-control" type="text" name="title" required>
				<textarea rows="20" cols="50"></textarea>
				<input type="submit" class="btn btn-default" value="Submit article">
				<input type=hidden name=action value='<c:out value="${Action.SAVE}"></c:out>'></input>
			</div>
		</form>	
	</div>
</body>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</html>