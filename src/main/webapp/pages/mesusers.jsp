<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.perou.wiki.controleurs.Action,com.perou.wiki.beans.Statut"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<title>Les Utilisateurs</title>
</head>


<body>
	
 	<jsp:include page="home.jsp"></jsp:include>
	
	<div class="container-fluid">
		<h2>Wiki Users</h2>
		<table class="table table-hover table-bordered" data-show-toggle="true">
			<tr class="info">
				<th>Username</th>
				<th>Password</th>
				<th>Email</th>
				<th>Nom</th>
				<th>Prenom</th>
				<th>Role</th>
				<th>Statut</th>
				<th>Change user status</th>
			</tr>
			<c:forEach items="${UserList}" var="entry">
				<tr
					class="
						<c:choose>
						    <c:when test="${entry.value.statut == 'ACTIVE'}">
						       success
						    </c:when>
						    <c:when test="${entry.value.statut == 'INACTIVE'}">
						        warning
						    </c:when>
						    <c:when test="${entry.value.statut == 'SUSPENDED'}">
						        danger
						    </c:when>
						    <c:otherwise>
						    </c:otherwise>
						</c:choose>
						">
					<th>${entry.value.username}</th>
					<th>${entry.value.password}</th>
					<th>${entry.value.mail}</th>
					<th>${entry.value.nom}</th>
					<th>${entry.value.prenom}</th>
					<th>${entry.value.role}</th>
					<th>${entry.value.statut}</th>
					<th>
							<div class="btn-group" role="group">
								<form method="post" action="userAction!doUpdateUser" name="frm">
									<button class="btn btn-success" name="statut"
										value="ACTIVE" onclick="{document.frm.submit();}">ACTIVE</button>
									<button class="btn btn-warning" name="statut"
										value="INACTIVE" onclick="{document.frm.submit();}">INACTIVE</button>
									<button class="btn btn-danger" name="statut"
										value="SUSPENDED" onclick="{document.frm.submit();}">SUSPENDED</button>
									<input type="hidden"name="action" value="userAction!doUpdateUser" /> 
									<input type="hidden" name="mail" value="${entry.value.mail}" />
							 </form>
					</th>
					<th>
						<form method="post" action="userAction!doDeleteUser">
								<button type="submit" class="btn btn-danger" name="" onclick="{document.frm.submit();}">Supprimer</button>
								<input type="hidden" name="action" value="userAction!doDeleteUser">
								<input type="hidden" name="mail" value="${entry.value.mail}">
						</form>
					</th>
			</c:forEach>
		</table>
	</div>
</body>

</html>