<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h1 class="modal-title">Login</h1>
		</div>
		<div class="modal-body">
			<form id ="login-form" method="post" action="LoginAction!doLogin">
				<div class="form-group">
					<fieldset>
						<div class="alert alert-danger hidden" role="alert" id="loginError">Wrong Username or Password, try again.</div>
						<b>Mail</b>
						<input class="form-control" type="text" name=mail ><br />
						<b>Password</b>
						<input class="form-control" type="password" name="password"> <br />
						<button  type="submit" id="btn-login" class="btn btn-success">Connexion</button>
						<br />
					</fieldset>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
		</div>
	</div>
</div>