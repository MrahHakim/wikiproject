<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.perou.wiki.controleurs.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Inscription</title>
</head>

<jsp:include page="menu.jsp"></jsp:include>

<body>
	<br>
	<br>
	<div class="container">
		<h2>Inscription</h2>
		<c:set var="enumValues" value="<%=Civilite.values()%>" />
		<form action="UserController" method="post">
			<div class="form-group">
				<label>Civilite : </label><SELECT name="civilite" size="1" class="form-control">
					<c:forEach items="${enumValues}" var="enumValue">
						<option value="${enumValue}">${enumValue}</option>
					</c:forEach>
				</SELECT>
			</div>
			<div class="form-group">
				<label for="nom">Nom:</label> <input type="text"
					class="form-control" id="nom" name="nom"
					placeholder="Entrez votre nom">
			</div>
			<div class="form-group">
				<label for="prenom">Prenom:</label> <input type="text"
					class="form-control" id="prenom" name="prenom"
					placeholder="Entrez votre prenom">
			</div>
			<div class="form-group">
				<label for="mail">Email:</label> <input type="email"
					class="form-control" id="mail" name="mail"
					placeholder="Entrez votre email">
			</div>
			<div class="form-group">
				<label for="login">Login:</label> <input type="text"
					class="form-control" id="login" name="username"
					placeholder="Entre votre login">
			</div>
			<div class="form-group">
				<label for="password">Mot de passe:</label> <input type="password"
					class="form-control" id="password" name="password"
					placeholder="Entrer votre mot de passe">
			</div>
			<div class="form-group">
				<label for="password">Confirmation:</label> <input type="password"
					class="form-control" id="password" name="password"
					placeholder="Confirmez votre mot de passe">
			</div>
			<input type="hidden" name="action"
				value="<c:out value='${Action.SAVE}'>Erreur</c:out>" />
			<button type="submit" class="btn btn-default">Valider</button>
		</form>
	</div>

</body>
</html>