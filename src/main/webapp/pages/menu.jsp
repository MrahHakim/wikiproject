<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<body>
	<nav class="navbar navbar-custom">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">WIKI</a>
			</div>		
			<ul class="nav navbar-nav">
						<li><button class="btn-link btn hidden" type="button" id="btn-home"><b>Home</b></button></li>
						<li><button class="btn-link btn hidden" type="button" id="btn-allUsers"><b>Liste Utilisateurs</b></button></li>
						<li><button class="btn-link btn hidden" type="button" id="btn-allArticles"><b>Liste Articles</b></button></li>
						<li><button class="btn-link btn hidden" type="button" id="btn-newArticle"><b>Cr�er Article</b></button></li>
						<li><button class="btn-link btn hidden" type="button" id="btn-logout" ><b>Logout</b></button></li>
						<li><button class="btn-link btn" type="button" id="btn-loginModal" data-toggle="modal" data-target="#loginModal"><b>Login</b></button></li>
						<li><button class="btn-link btn" type="button" id="btn-registerModal" data-toggle="modal" data-target="#registerModal"><b>Register</b></button></li>						
			</ul>
		</div>
	</nav>
	<div id="loginModal" class="modal fade" role="dialog">
		<jsp:include page="login.inc.jsp"></jsp:include>
	</div>
	<div id="registerModal" class="modal fade" role="dialog">
		<jsp:include page="register.inc.jsp"></jsp:include>
	</div>
</body>