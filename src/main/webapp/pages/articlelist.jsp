<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.perou.wiki.controleurs.Action,com.perou.wiki.controleurs.Civilite"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-table.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
.myButton {
	background-color: #768d87;
	-moz-border-radius: 28px;
	-webkit-border-radius: 28px;
	border-radius: 28px;
	border: 1px solid #566963;
	display: inline-block;
	cursor: pointer;
	color: #ffffff;
	font-family: Arial;
	font-size: 21px;
	padding: 15px 61px;
	text-decoration: none;
	text-shadow: 0px 1px 20px #2b665e;
}

.myButton:hover {
	background-color: #6c7c7c;
}

.myButton:active {
	position: relative;
	top: 1px;
}
</style>
<title>Articles</title>
</head>
<jsp:include page="menu.jsp"></jsp:include>
<body>
	<div class="container-fluid">
		<h2>list Articles</h2>
		<table class="table table-hover table-bordered"
			data-show-toggle="true">
			<tr class="info">
				<th>ID</th>
				<th>Titre</th>
				<th>Contenu</th>
				<th>Score</th>
				<th>Etat</th>
				<th>Date de Publictaion</th>
				<th>Staut</th>

			</tr>
			<c:forEach items="${articlelist}" var="entry">
				<tr
					class="
 						<c:choose> 
						    <c:when test="${entry.value.status == 'PUBLIE'}">
						       success 
 						    </c:when> 
						    <c:when test="${entry.value.status == 'ATTENTE'}">
 						        warning 
 						    </c:when> 
						    <c:when test="${entry.value.status == 'REFUSE'}">
 						        danger 
						    </c:when> 
 						    <c:otherwise> 
 						    </c:otherwise> 
 						</c:choose>
						">
					<th>${entry.value.idArticle}</th>
					<th>${entry.value.titre}</th>
					<th>${entry.value.contenu}</th>
					<th>${entry.value.score}</th>
					<th>${entry.value.etat}</th>
					<th>${entry.value.dateArticle}</th>
					<th>${entry.value.status}</th>
			</c:forEach>
		</table>
		<a href="http://localhost:8080/wiki/pages/articleForm.jsp"
			class="myButton">Ajouter un Article</a>
</body>
</html>