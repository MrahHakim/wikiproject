package com.perou.wiki.beans;

public enum Role{
	ADMIN, 
	AUTEUR, 
	CONTRIBUTEUR;
}
