package com.perou.wiki.beans;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class ArticleDao {

	private static Session currentSession;
	private static Transaction currentTransaction;

	public void ArticleDao() {
	}

	public static Session openCurrentSession() {
		currentSession = HibernateUtil.currentSession();
		return currentSession;
	}

	public static Session openCurrentSessionwithTransaction() {
		currentSession = HibernateUtil.currentSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public static void closeCurrentSession() {
		HibernateUtil.closeSession();
	}

	public static void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		HibernateUtil.closeSession();
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	public void addArticle(Article article) {
		UserDao.openCurrentSessionwithTransaction();
		getCurrentSession().save(article);
		UserDao.closeCurrentSessionwithTransaction();
	}

	public void deleteArticle(Article article) {
		UserDao.openCurrentSessionwithTransaction();
		getCurrentSession().delete(article);
		UserDao.closeCurrentSessionwithTransaction();
	}

	public void updateArticle(Article article) {
		UserDao.openCurrentSessionwithTransaction();
		getCurrentSession().update(article);
		UserDao.closeCurrentSessionwithTransaction();
	}

	public HashMap<String, Article> getAllArticle() {
		UserDao.openCurrentSessionwithTransaction();
		HashMap<String, Article> articles = new HashMap<String, Article>();
		Query query = getCurrentSession().getNamedQuery("findAllArticle");
		List<Article> articleList = query.list();
		for (Article a : articleList) {
			articles.put(a.getTitre(), a);
		}
		UserDao.closeCurrentSessionwithTransaction();
		return articles;

	}

	public HashMap<String, Article> getArticleByUser(int idUser) {
		UserDao.openCurrentSessionwithTransaction();
		HashMap<String, Article> articles = new HashMap<String, Article>();
		Query query = getCurrentSession().getNamedQuery("findArticleByUser").setParameter("idUser", idUser);
		List<Article> articleList = query.list();
		for (Article a : articleList) {
			articles.put(a.getTitre(), a);
		}
		UserDao.closeCurrentSessionwithTransaction();
		return articles;
	}

}
