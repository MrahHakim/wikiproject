package com.perou.wiki.beans;

public enum Statut {

	ACTIVE,
	INACTIVE,
	SUSPENDED;
	
}
