package com.perou.wiki.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.perou.wiki.beans.EtatArticle;;

@Entity
@Table(name = "article")
@NamedQueries({ @NamedQuery(name = "findAllArticle", query = "FROM Article c"),
		@NamedQuery(name = "findArticleByUser", query = "FROM Article c WHERE c.user.idUser = :idUser") })
public class Article {

	@Id
	private int idArticle;

	@Column(name = "score")
	private int score;

	@Column(name = "titre")
	private String titre;

	@Column(name = "contenu")
	private String contenu;

	@Column(name = "dateArticle")
	private Date dateArticle;

	@Column(name = "etat")
    @Enumerated(EnumType.STRING)
	private EtatArticle etat;

	@ManyToOne
	@JoinColumn(name = "idUser")
	private User user;

	public Article(int idArticle, int score, String titre, String contenu, Date dateArticle, EtatArticle etat,
			User user) {
		this.idArticle = idArticle;
		this.score = score;
		this.titre = titre;
		this.contenu = contenu;
		this.dateArticle = dateArticle;
		this.etat = etat;
		this.user = user;
	}

	public Article(int score, String titre, String contenu, Date dateArticle, User user) {
		super();
		this.score = score;
		this.titre = titre;
		this.contenu = contenu;
		this.dateArticle = dateArticle;
		this.etat = EtatArticle.ATTENTE;
		this.user = user;
	}

	public Article() {
	}

	public int getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(int idArticle) {
		this.idArticle = idArticle;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Date getDateArticle() {
		return dateArticle;
	}

	public void setDateArticle(Date dateArticle) {
		this.dateArticle = dateArticle;
	}

	public EtatArticle getEtat() {
		return etat;
	}

	public void setEtat(EtatArticle etat) {
		this.etat = etat;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
