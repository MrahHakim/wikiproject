package com.perou.wiki.beans;

public enum Civilite {
	
	MONSIEUR,
	MADAME,
	DOCTEUR,
	MAITRE;
}
