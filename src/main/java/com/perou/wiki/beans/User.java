package com.perou.wiki.beans;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="user")
public class User {
	
	@Id
	private int idUser;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="prenom")
	private String prenom;
	
	@Column(name="mail")
	private String mail;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@Column(name="role")
    @Enumerated(EnumType.STRING)
	private Role role;
	
	@Column(name="civilite")
    @Enumerated(EnumType.STRING)
	private Civilite civilite;
	
	@Column(name="statut")
    @Enumerated(EnumType.STRING)
	private Statut statut;
	

	@OneToMany(mappedBy="user",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Article> mesArticles;

	
	public User(int idUser, String nom, String prenom, String username, String password, String mail, Role role,
			Civilite civilite, Statut statut) {
		super();
		this.idUser = idUser;
		this.nom = nom;
		this.prenom = prenom;
		this.username = username;
		this.password = password;
		this.mail = mail;
		this.role = role;
		this.civilite = civilite;
		this.statut = statut;
	}
	
	
	public User(String nom, String prenom, String username, String password, String mail, Civilite civilite) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.username = username;
		this.password = password;
		this.mail = mail;
		this.role = Role.CONTRIBUTEUR;
		this.civilite = civilite;
		this.statut = Statut.ACTIVE;
	}
	
	public User() {
		// TODO Auto-generated constructor stub
	}


	public void setStatut(Statut statut){
		this.statut = statut;
	}
	
	public Statut getStatut(){
		return statut;
	}
	
	public void setCivilite(Civilite civilite){
		this.civilite = civilite;
	}
	
	public Civilite getCivilite(){
		return civilite;
	}
	
	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.nom = mail;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Article> getMesArticles() {
		return mesArticles;
	}

	public void setMesArticles(List<Article> mesArticles) {
		this.mesArticles = mesArticles;
	}

}
