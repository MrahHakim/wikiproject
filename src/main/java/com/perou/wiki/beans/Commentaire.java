package com.perou.wiki.beans;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="commentaire")
@NamedQueries({
    @NamedQuery(name="findAllComments",
                query="SELECT c FROM Commentaire c")
}) 
public class Commentaire {
	
	@Id
	private int idCommentaire;
	private int note;
	private Date dateCommentaire;
	private String contenuCommentaire;
	private int idArticle;
	private List<Commentaire> mesCommentaires;
	
	public Commentaire(int idCommentaire, int note, Date dateCommentaire, String contenuCommentaire, int idArticle) {
		super();
		this.idCommentaire = idCommentaire;
		this.note = note;
		this.dateCommentaire = dateCommentaire;
		this.contenuCommentaire = contenuCommentaire;
		this.idArticle =  idArticle;
	}
	
	public Commentaire(){
	}

	public int getIdCommentaire() {
		return idCommentaire;
	}

	public void setIdCommentaire(int idCommentaire) {
		this.idCommentaire = idCommentaire;
	}

	public int getNote() {
		return note;
	}

	public void setNote(int note) {
		this.note = note;
	}

	public Date getDateCommentaire() {
		return dateCommentaire;
	}

	public void setDateCommentaire(Date dateCommentaire) {
		this.dateCommentaire = dateCommentaire;
	}

	public String getContenuCommentaire() {
		return contenuCommentaire;
	}

	public void setContenuCommentaire(String contenuCommentaire) {
		this.contenuCommentaire = contenuCommentaire;
	}
	
	
}
