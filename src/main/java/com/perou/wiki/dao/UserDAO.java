package com.perou.wiki.dao;

import java.util.List;

import javax.naming.ldap.ManageReferralControl;
import javax.net.ssl.ManagerFactoryParameters;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.perou.wiki.beans.User;

@Component("userdao")
@Repository
public class UserDAO extends AbstractDAO<User> implements IUserDAO {

	public UserDAO() {
		setClazz(User.class);
	}

	@Override
	public User getUserByMail(String mail) {
		Query query = entityManager.createNativeQuery("SELECT c FROM user c WHERE c.mail = ?");  
		query.setParameter(1, mail); 
		List<User> users = query.getResultList();
		if(!users.isEmpty()){
			User user = users.get(0);
			return user;
		}
		return null;
	}	
	
}
