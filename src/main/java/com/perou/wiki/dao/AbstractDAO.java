package com.perou.wiki.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractDAO<T> {

	private Class<T> myClass;

	@PersistenceContext
	EntityManager entityManager;

	public final void setClazz(Class<T> clazzToSet) {
		this.myClass = clazzToSet;
	}

	public T findOne(int id) {
		return entityManager.find(myClass, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return entityManager.createQuery("from " + myClass.getName()).getResultList();
	}

	public void create(T entity) {
		entityManager.persist(entity);
	}

	public void update(T entity) {
		 entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entity);
	}

	public void deleteById(int entityId) {
		T entity = findOne(entityId);
		delete(entity);
	}
}
