package com.perou.wiki.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.perou.wiki.beans.User;

public interface IUserDAO {

	User findOne(int id);

    List<User> findAll();

    void create(User entity);

    void update(User entity);

    void delete(User entity);

    void deleteById(int entityId);
    
////    @Query("SELECT p FROM User p WHERE p.mail = :mail")
//    User getUserByMail(@Param("mail") String mail); 
    User getUserByMail(String mail);
}
