//package com.perou.wiki.controleurs;
//
//import java.io.IOException;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import com.perou.wiki.beans.UserDao;
//
//@WebServlet(displayName = "LoginController", urlPatterns = { "/LoginController", "/Login" })
////@WebServlet(displayName="Login",urlPatterns={"/login"})
//public class LoginController extends HttpServlet {
//	private static final long serialVersionUID = 1L;
//	private String target;
//
//	UserDao dao = new UserDao();
//
//	public LoginController() {
//	}
//
//	public String getTarget() {
//		return target;
//	}
//
//	public void setTarget(String target) {
//		this.target = target;
//	}
//
//	protected void doGet(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		Action action = Action.valueOf(request.getParameter("action"));
//		switch (action) {
//		case CONNECT:
//			doLogin(request, response);
//			break;
//
//		case DISCONNECT:
//			doLogout(request, response);
//			break;
//
//		default:
//			setTarget("pages/menu.jsp");
//
//		}
//
//		doDispatch(request, response);
//	}
//
//	protected void doPost(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
////		Action action = Action.valueOf(request.getParameter("action"));
////		switch (action) {
////		case CONNECT:
////			doLogin(request, response);
////			break;
////
////		case DISCONNECT:
////			doLogout(request, response);
////			break;
////
////		default:
////			setTarget("pages/menu.jsp");
////
////		}
////	
////		doDispatch(request, response);
//
//	}
//
//	protected void doLogout(HttpServletRequest request, HttpServletResponse response) {
//		HttpSession session = request.getSession();
//		session.invalidate();
//		setTarget("pages/menu.jsp");
//
//	}
//
//	protected void doLogin(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//
//		String mail = (String) request.getParameter("mail");
//		String pwd = (String) request.getParameter("pwd");
//
//		if (dao.getUserAtConnection(mail, pwd) != null) {
//
//			setTarget("pages/userhome.jsp");
//
//			doDispatch(request, response);
//
//		} else {
//
//			String message = "";
//			if (mail != null || pwd != null)
//				message = "Login error or you do not have an account !";
//			request.setAttribute("message", message);
//		}
//
//	}
//
//	protected void doDispatch(HttpServletRequest request, HttpServletResponse response)
//			throws ServletException, IOException {
//		request.getRequestDispatcher(target).forward(request, response);
//	}
//
//}