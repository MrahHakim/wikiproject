package com.perou.wiki.controleurs;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.perou.wiki.beans.User;

@WebServlet(displayName = "Accueil", urlPatterns = { "/accueil", "/home" })
public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Accueil() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("pages/menu.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
