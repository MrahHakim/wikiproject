package com.perou.wiki.controleurs.struts;

import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.perou.wiki.beans.User;
import com.perou.wiki.dao.UserDAO;

@SuppressWarnings("serial")
@ParentPackage("json-default")
public class LoginAction extends ActionSupport implements SessionAware {
	private static final String LOGED = "loged";
	private static final String NOLOGED = "nologed";
	private Map<String, Object> sessionMap;
	private String mail;
	private String password;
	private User user;
	


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setSession(Map<String, Object> arg0) {
		this.sessionMap = arg0;

	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	@Action(value="doLogin", results={@Result(name = "success", type = "json")})
	public String doLogin() {
		UserDAO dao = new UserDAO();
		user = dao.getUserByMail(mail);
		System.out.println("LoginAction.doLogin()" + user.getPrenom());
		if (user != null && user.getPassword().equals(password)) {
				sessionMap.put("userSession", user);
				return SUCCESS;
			}
			else
				return SUCCESS;
	}

	@Action(value="doLogout", results={@Result(name = "success", type = "json")})
	public String doLogout() {
		sessionMap.clear();
		return SUCCESS;
	}

}