package com.perou.wiki.controleurs.struts;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
@Action(value = "welcome")
@Result(name = "success", location = "/pages/home.jsp")
public class WelcomeAction extends ActionSupport{

	public String execute(){
		return SUCCESS;
	}
}
