package com.perou.wiki.controleurs.struts;

import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.opensymphony.xwork2.ActionSupport;
import com.perou.wiki.beans.Civilite;
import com.perou.wiki.beans.Role;
import com.perou.wiki.beans.Statut;
import com.perou.wiki.beans.User;
import com.perou.wiki.dao.UserDAO;

@SuppressWarnings("serial")
public class UserAction extends ActionSupport implements SessionAware{

	private Map<String, Object> sessionMap;
	private HashMap<String, User> UserList;

	private String nom;
	private String prenom;
	private String username;
	private String password;
	private String mail;
	private Role role;
	private Civilite civilite;
	private Statut statut;
	private String message;
	
	ApplicationContext context = new ClassPathXmlApplicationContext();
	

	public HashMap<String, User> getUserList() {
		return UserList;
	}

	public void setUserList(HashMap<String, User> userList) {
		UserList = userList;
	}
	
	public Map<String, Object> getSession() {
		return sessionMap;
	}
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.sessionMap = session;
	}

	@SuppressWarnings("unchecked")
	@Action(value="doSaveUser", results={@Result(name = "success", type = "json")})
	public String doSaveUser() {
		UserDAO dao = (UserDAO) context.getBean("userdao");
		User myUser = new User(nom, prenom, mail, username, password, civilite);
		if (dao.getUserByMail(mail) == null) {
			dao.create(myUser);
			this.UserList = (HashMap<String, User>) dao.findAll();
			sessionMap.put("userSession", myUser);
			message = "Enregistrement effectuer avec succes .";
			return SUCCESS;
		} else 
			message = "Enregristrement echec";
			return SUCCESS;
		
		}


	@SuppressWarnings("unchecked")
	@Action(value="doListAllUsers", results={@Result(name = "success", type = "json")})
	public String doListAllUsers() {
		UserDAO dao = new UserDAO();
		UserList = (HashMap<String, User>) dao.findAll();
		return SUCCESS;
	}

	@Action(value="doDeleteUser", results={@Result(name = "success", type = "json")})
	public String doDeleteUser() {
		UserDAO dao = new UserDAO();
		User myUser = new User(nom, prenom, mail, username, password, civilite);
		dao.delete(myUser);
		return SUCCESS;
	}

	@Action(value="doUpdateUser", results={@Result(name = "success", type = "json")})
	public String doUpdateUser() {
		System.out.println("UserAction.doUpdateUser() debut");
		UserDAO dao = new UserDAO();
		System.out.println("UserAction.doUpdateUser() dao = " + dao);
		User monUser = dao.getUserByMail(mail);
		System.out.println("UserAction.doUpdateUser() monuser = " + monUser);
		Statut newStatus = statut;
		System.out.println("UserAction.doUpdateUser() newStatus = " + newStatus);
		monUser.setStatut(newStatus);
		System.out.println("UserAction.doUpdateUser() fin" + monUser);
		dao.update(monUser);
		System.out.println("update OK");
		this.UserList = (HashMap<String, User>) dao.findAll();
		System.out.println("get all users OK");
		return SUCCESS;
	}

	public Map<String, Object> getSessionMap() {
		return sessionMap;
	}

	public void setSessionMap(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public Statut getStatut() {
		return statut;
	}

	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
