package com.perou.wiki.controleurs;

public enum Action {

	SAVE,
	DELETE,
	UPDATE,
	LIST,
	FIND,
	EDIT,
	CREATE,
	LOGIN,
	LOGOUT;
}
