package com.perou.wiki.controleurs;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.perou.wiki.beans.Article;
import com.perou.wiki.beans.ArticleDao;

@WebServlet(displayName = "ArticleController", urlPatterns = { "/articlecontroller", "/article" })
public class ArticleController extends AbstractServlet {
	private static final long serialVersionUID = 1L;
	private static Map<Integer, Article> articleList = new HashMap<Integer, Article>();
	private ArticleDao dao = new ArticleDao();

	public ArticleController() {
	}

	protected void doFind(HttpServletRequest request, HttpServletResponse response) {
//		String username=(String)request.getParameter("username");
//		Article article=dao.getArticleByUser(username);
//				request.setAttribute("username", username);
	}

	protected void doList(HttpServletRequest request, HttpServletResponse response) {
		
		
		System.out.println("ArticleController.doList()");
		request.setAttribute("articleList", articleList);
		setTarget("pages/articleList.jsp");

	}

	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) {

	}

	protected void doSave(HttpServletRequest request, HttpServletResponse response) {
//		String titre = (String) request.getParameter("titre");
//		String contenu = (String) request.getParameter("contenu");
//		String dateArticle = (String) request.getParameter("dateArticle");
//		int score = Integer.parseInt(request.getParameter("score"));
//		int etat=Integer.parseInt(request.getParameter("etat"));
//		
//		StatutArticle statutArticle = StatutArticle.valueOf((String) request.getParameter("Status"));
//			
//		
//		Article article =new Article(titre, contenu, dateArticle, etat, score);		
//
//		dao.addArticle(article);
//		
//		
//		
//		setTarget("pages/articlelist.jsp");		

	}

	protected void doEdit(HttpServletRequest request, HttpServletResponse response) {

	}

	protected void doDel(HttpServletRequest request, HttpServletResponse response) {

	}

}
