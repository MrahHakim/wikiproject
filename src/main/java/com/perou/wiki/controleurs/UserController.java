package com.perou.wiki.controleurs;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.perou.wiki.beans.*;
import com.perou.wiki.beans.Civilite;
@WebServlet(displayName = "UserController", urlPatterns = { "/UserController", "/user" })
public class UserController extends AbstractServlet {
	private static final long serialVersionUID = -5732747893904459131L; 
	private static Map<String, User> mesUsers;
	UserDao dao = new UserDao();
	protected void doFind(HttpServletRequest request, HttpServletResponse response) {
	}
	protected void doList(HttpServletRequest request, HttpServletResponse response) {

		mesUsers = dao.getAllUsers();
		if (mesUsers != null)
			request.setAttribute("users", mesUsers);

		setTarget("pages/mesusers.jsp");
	}

	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) {

		String mail = ((String) request.getParameter("mail")).trim();
		User monUser = dao.getUserById(mesUsers.get(mail).getIdUser());
		Statut newStatus = Statut.valueOf((String) request.getParameter("statut"));
		monUser.setStatut(newStatus);
		dao.updateUser(monUser);
		doList(request, response);
	}

	protected void doSave(HttpServletRequest request, HttpServletResponse response) {
//		String nom = (String) request.getParameter("nom");
//		String prenom = (String) request.getParameter("prenom");
//		String mail = (String) request.getParameter("mail");
//		String username = (String) request.getParameter("username");
//		String password = (String) request.getParameter("password");
//		Civilite civilite = Civilite.valueOf(request.getParameter("civilite"));
//
//		User myUser = new User(nom, prenom, mail, username, password, civilite);
//		dao.addUser(myUser);
//		mesUsers.put(myUser.getMail(), myUser);
//
//		setTarget("pages/menu.jsp");
//
}

	protected void doEdit(HttpServletRequest request, HttpServletResponse response) {

		setTarget("pages/formulaire.jsp");
	}

	protected void doDel(HttpServletRequest request, HttpServletResponse response) {
//		String mail = request.getParameter("mail");
//		User monUser = dao.getUserById(mesUsers.get(mail).getIdUser());
//		dao.deleteUser(monUser.getIdUser());
//		doList(request, response);
	}

}
