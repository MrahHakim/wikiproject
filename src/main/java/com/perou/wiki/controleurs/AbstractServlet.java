package com.perou.wiki.controleurs;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.perou.wiki.beans.Civilite;
import com.perou.wiki.beans.Statut;
import com.perou.wiki.beans.User;

public abstract class AbstractServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String target;

	public AbstractServlet() {
		super();
	}
	
	protected String getTarget() {
		return target;
	}

	protected void setTarget(String target) {
		this.target = target;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Action action = Action.valueOf(request.getParameter("action"));

		switch (action)
		{

		case CREATE:
		case EDIT:
			doEdit(request, response);
			break;

		case FIND:
			doFind(request, response);
			break;

		case LIST:
		default:
			doList(request, response);

		}
		doDispatch(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Action action = Action.valueOf(request.getParameter("action"));

		switch (action) 
		{
		case SAVE:
			doSave(request, response);
			break;

		case DELETE:
			doDel(request, response);
			break;

		case UPDATE:
			doUpdate(request, response);
			break;

		default:
			doList(request, response);

		}

		doDispatch(request, response);
		
	}
		
		protected abstract void doFind(HttpServletRequest request, HttpServletResponse response) ;
		protected abstract void doList(HttpServletRequest request, HttpServletResponse response);
		protected abstract void doUpdate(HttpServletRequest request, HttpServletResponse response);
		protected abstract void doSave(HttpServletRequest request, HttpServletResponse response);
		protected abstract void doEdit(HttpServletRequest request, HttpServletResponse response);
		protected abstract void doDel(HttpServletRequest request, HttpServletResponse response);
		
		protected void doDispatch(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			request.getRequestDispatcher(target).forward(request, response);
		}	

}
