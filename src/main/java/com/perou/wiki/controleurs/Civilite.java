package com.perou.wiki.controleurs;

public enum Civilite {
	MONSIEUR,
	MADAME,
	MAITRE,
	DOCTEUR;
}
