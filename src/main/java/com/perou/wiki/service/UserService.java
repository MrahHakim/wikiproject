package com.perou.wiki.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perou.wiki.beans.User;
import com.perou.wiki.dao.IUserDAO;
import com.perou.wiki.dao.UserDAO;


@Service
@Transactional
public class UserService implements IUserDAO {

	@Autowired
	UserDAO dao;
	
	@Override
	public User findOne(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<User> findAll() {
		return dao.findAll();
	}

	@Override
	public void create(User user) {
		dao.create(user);
	}

	@Override
	public void update(User user) {
		dao.update(user);
	}

	@Override
	public void delete(User user) {
		dao.delete(user);

	}

	@Override
	public void deleteById(int UserID) {
		dao.deleteById(UserID);

	}

	@Override
	public User getUserByMail(String mail) {
		return dao.getUserByMail(mail);
	}

}
